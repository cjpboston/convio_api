#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""The setup script."""
# Note to self: to build new egg, navigate to directory, then run "python setup.py sdist"

from setuptools import setup, find_packages

with open("README.md") as readme_file:
    readme = readme_file.read()

requirements = ["zeep", "lxml", "requests"]

setup(
    name="convio_api",
    version="2.0.1",
    description="Wrapper around Convio/Luminate webservices SOAP api. Download only. ",
    long_description=readme,
    author="danylo @ cjp",
    author_email="danylok@cjp.org",
    url="https://gitlab.com/CJPBoston/convio_api",
    packages=find_packages(),
    install_requires=requirements,
    license="GNU General Public License v3",
    zip_safe=False,
    keywords="convio_api, convio, luminate, blackbaud",
    classifiers=[
        "Development Status :: 3 - Beta",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Natural Language :: English",
        "Programming Language :: Python :: 3.6",
    ],
)
