import logging.config
from zeep import exceptions, Client, Transport
from zeep.helpers import serialize_object
from datetime import datetime
from time import sleep
from math import ceil
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry


def set_soap_log_level(level="INFO"):
    """
    Can be used to view raw XML request and response. If set to DEBUG, outputs to console/jupyter notebook. 
    I suggest using jupyter or editing this to dump into a log file.

    :param level: one of ['ERROR', 'DEBUG', 'INFO', 'WARNING']"""
    assert level in ["ERROR", "DEBUG", "INFO", "WARNING"]
    logging.config.dictConfig(
        {
            "version": 1,
            "formatters": {"verbose": {"format": "%(name)s: %(message)s"}},
            "handlers": {
                "console": {
                    "level": "DEBUG",
                    "class": "logging.StreamHandler",
                    "formatter": "verbose",
                }
            },
            "loggers": {
                "zeep.transports": {
                    "level": level,
                    "propagate": True,
                    "handlers": ["console"],
                }
            },
        }
    )


class ConvioAPI:
    """
    Wrapper around the zeep soap client connecting to Convio's webservices api. Download only, developed with an email data focus. 
    This is the pull side of a datawarehouse sync.
    Full API documented @ http://open.convio.com/webservices/
    """

    CHANGE_TYPES = ["Inserts", "Updates", "Deletes"]
    _PAGE_SIZE = (
        200
    )  # API has bug where pagination is done according to the very first PageSize param.

    def __init__(
        self,
        username: str,
        password: str,
        wsdl: str,
        partition: int = 1001,
        download_delay: float = 0.0,
        proxies=None,
        transport_kwargs={},
        client_kwargs={},
        retry_kwargs={},
    ):
        """Initializes a logged in ConvioAPI object. Handles session management.
        Remember to whitelist your IP address on the luminate api config page, or use a whitelisted proxy.
        
        Arguments:
            username {str} -- username
            password {str} -- password
            wsdl {str} -- url to wsdl (usually IP whitelist protected)
        
        Keyword Arguments:
            partition {int} -- Luminate partition.  (default: {1001})
            download_delay {float} -- sleep between API calls, in seconds. (default: {0.0})
            proxies {[type]} -- Proxies argument to pass to requests session. (default: {None})
            transport_kwargs {dict} -- keyword args to pass to zeep.Transport object (default: {{}})
            client_kwargs {dict} -- keyword args to pass to zeep.Client object (default: {{}})
            retry_kwargs {dict} -- keyword args to pass to urllib3.util.retry.Retry object. If {} then not used. 
                Retry object is mounted on the Transport.session via requests.adapters.HTTPAdapter. 
                If passed, used to handle read/connection/etc. timeouts. (default: {{}}) 
        Raises:
            n/a
        
        Returns:
            [ConvioAPI] -- [returns self.]
        """
        transport = Transport(**transport_kwargs)
        if proxies:
            transport.session.proxies.update(proxies)
        if retry_kwargs:
            retry = Retry(**retry_kwargs)
            adapter = HTTPAdapter(max_retries=retry)
            transport.session.mount("http://", adapter)
            transport.session.mount("https://", adapter)
        self.client = Client(wsdl, transport=transport, **client_kwargs)
        self._username = username
        self._password = password
        self._session = None
        self._header = {"_soapheaders": dict()}
        self._login()
        self._partition = partition
        self._delay = download_delay

    def _login(self):
        """Attemps to log in, updates Session header if successful."""
        self._session = self.client.service.Login(
            UserName=self._username, Password=self._password
        )
        self._header["_soapheaders"]["Session"] = self._session
        logging.info("SUCCESS: Session restarted")

    def _session_safe(self, func, **kwargs):
        """
        Calls func(**kwargs), catches session timeout Fault and tries once to restart session. Handles download_delay, header. 
        Every call should go through here.

        :param self:
        :param func: function to be called that might raise Session Fault exception. 
        :param kwargs: args to pass to function. 
        :return: Returns whatever func returns. 
        """
        # self._header = {'_soapheaders': {'Session': 'fake_test_session'}} # debug test.
        # every other outbound method (except _login) goes through _session_safe
        sleep(self._delay)
        try:
            return func(**kwargs, **self._header)
        except exceptions.Fault as fault:
            if fault.code == "fns:SESSION":
                logging.info("Session error, restarting session")
                print("session error, restarting session.")
                self._login()
                sleep(.5)
                return func(**kwargs, **self._header)
            else:
                raise

    def query(self, query: str, from_page: int = 1):
        """
        Calls Query api method with query string. Returns 

        :param query: query in sql format ('select * from Constituent'), see docs for more details. 
        :param from_page: Defaults to 1, page from which to start pulling. Use for debugging. 
        :return: List[responses]
        """
        responses = self._yield_paginate_until_empty(
            service=self.client.service.Query, QueryString=query, page=from_page
        )
        for response in responses:
            yield serialize_object(response)

    def get_partitions(self):
        """
        Returns list of available partitions. PartitionID is used as a key for other operations. 

        :return: List[Partitions]
        """
        return [p for p in self.query("select * from Partition")]

    def get_synchronization_sessions(
        self,
        partition=None,
        end_date_between=(
            datetime(1970, 1, 1),
            datetime(9999, 12, 31, 23, 59, 59, 9999),
        ),
    ):
        """
        Returns all synchronization sessions for a given partition. Should only ever be one active session. 

        :param partition: PartitionID, defaults to self._partition
        :param end_date_between: tuple(start_datetime, end_datetime) defaults to (start_of_time, end_of_time)
        :return: List of dicts of all sync sessions for given partition.

        API Start/end dates behavior doesn't follow documentation, so default values make it more predictable. 
        """
        # Date filter includes sync sessions that either started or ended between two dates.
        # Default api behavior is misdocumented. More predicable:
        partition = partition or self._partition
        syncs = self._session_safe(
            self.client.service.GetSynchronizationLog,
            PartitionId=partition,
            Start=end_date_between[0],
            End=end_date_between[1],
        )
        return serialize_object(syncs)

    def start_sync_session(self, start_date=None, end_date=None, partition=None):
        """
        Starts synchronization session for a partition. GetIncrementalChanges returns changes between start and end dates.

        :param start_date: Datetime from which to track changes, defaults to end of last sync session of partition.
        :param end_date: Datetime until which to track changes, defaults to current time. 
        :param partition: Constituent partition, defaults to self._partition
        :return: sync session. 
        """
        # If start_date is not specified, it's set to the end date of the last successful sync.
        # If end_Date is not specified, it's set to the current system time @ server.
        partition = partition or self._partition
        sync = self._session_safe(
            self.client.service.StartSynchronization,
            PartitionId=partition,
            Start=start_date,
            End=end_date,
        )
        return serialize_object(sync)

    def end_sync_session(self, partition=None):
        """
        Ends current (active) sync session for partition. 
        :param partition: PartitionID, defaults to self._partition
        :return: ?
        """
        partition = partition or self._partition
        sync = self._session_safe(
            self.client.service.EndSynchronization, PartitionId=partition
        )
        return serialize_object(sync)

    def describe_record_type(
        self, record_type: str, parent_type: str = None, parent_field: str = None
    ):
        """
        Returns available fields and operations for record type. 

        :param record_type: Name of recordtype defined by convio api. 
        :param parent_type: Parent recordtime, i.e. Constituent.Name
        :param parent_field: ?
        :return:
        """
        desciption = self._session_safe(
            self.client.service.DescribeRecordType,
            RecordType=record_type,
            ParentType=parent_type,
            ParentField=parent_field,
        )
        return serialize_object(desciption)

    def _yield_paginate_until_empty(
        self,
        service,
        page: int = 1,
        num_expected_pages: int = None,
        verbose: bool = False,
        **kwargs,
    ):
        """
        Given a webservices endpoint that supports pagination, yields that service until no more pages are returned.

        :param service: func of type client.service.ServiceName, must support pagination.
        :param page: Starting page, defaults to 1. 
        :param num_expected_pages: Expected final value of page.
        :param kwargs: args that not header, page, page_size, passed to service
        :return: List[responses]

        Uses self._PAGE_SIZE to paginate. Don't mess with it, api buggy if it changes during a sync_session. 
        Session safe. 
        """
        # known API bug: Pagination is done on the very fist page_size call,
        # after that it returns the top_n=page_size records from pages created by prior page_size.
        response_size = self._PAGE_SIZE
        num_expected_pages = num_expected_pages or 0
        # can't compare to page_size because pagination is unreliable. 1 extra API call.
        # expected_num_pages because API occasionally returns an empty page.
        while response_size > 0 or page < num_expected_pages:
            items = self._session_safe(
                service, Page=page, PageSize=self._PAGE_SIZE, **kwargs
            )
            response_size = len(items)
            if verbose:
                print(f"page: {page} had {response_size} items, {len(response)} total")
            page += 1
            yield items

    def generate_incremental_changes(
        self,
        record_type: str,
        fields: [str],
        change_type: str,
        partition: int = None,
        from_page: int = 1,
        verbose: bool = False,
    ):
        """
        Gets all changes to record_type that occured during the sync session. Updates/Inserts/Deletes
        
        :param record_type: String name of record type defined by API. Might be custom.
        :param fields: List of string field names that exist in record type. 
        :param change_type: One of 'Updates' 'Inserts' or 'Deletes'
        :param partition: Constituent partition, defaults to self._partition
        :param from_page: Starts pagination from page, use for debugging. # starts at 1 
        :return: generator of pages: List[responses]

        Session safe, strips zeep_object wrapper.
        """
        partition = partition or self._partition
        assert (
            change_type in self.CHANGE_TYPES
        ), f"Change type must be one in {self.CHANGE_TYPES}"
        service = getattr(self.client.service, "GetIncremental" + change_type)
        num_expected_records = self.get_incremental_changes_counts(
            record_type, change_type, partition
        )
        num_expected_pages = ceil(num_expected_records / self._PAGE_SIZE)
        num_expected_records = num_expected_records - (from_page - 1) * self._PAGE_SIZE
        if verbose:
            print(f"Pulling {num_expected_records}")
        changes = self._yield_paginate_until_empty(
            service=service,
            page=from_page,
            num_expected_pages=num_expected_pages,
            verbose=verbose,
            RecordType=record_type,
            Field=fields,
            PartitionId=partition,
        )
        for change in changes:
            yield serialize_object(change)

    def get_incremental_changes_counts(
        self, record_type: str, change_type: str, partition: int = None
    ):
        """
        Returns Count of changes to record_type that occured during the sync session. Updates/Inserts/Deletes
        
        :param record_type: String name of record type defined by API. Might be custom.
        :param change_type: One of 'Updates' 'Inserts' or 'Deletes'
        :param partition: Constituent partition, defaults to self._partition
        :return: Int
        """
        partition = partition or self._partition
        assert (
            change_type in self.CHANGE_TYPES
        ), f"Change type must be one in {self.CHANGE_TYPES}"
        service = getattr(self.client.service, "GetIncremental" + change_type + "Count")
        count = self._session_safe(
            service,
            RecordType=record_type,
            PartitionId=partition,
            PageSize=self._PAGE_SIZE,
        )
        return serialize_object(count)
