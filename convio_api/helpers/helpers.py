from decimal import Decimal
from collections import OrderedDict
from datetime import date, datetime


def json_serialize_handler(obj):
    """JSON serializer for objects of classes decimal.Decimal, datetime.date, datetime.datetime
    For use with json.dumps(object, default=json_serialize_handler) when 
    
    Arguments:
        obj {Any} -- Object that raises TypeError when json.dump attempts to serialize it.
    
    Raises:
        TypeError: Raised if we don't handle obj json serialization 
    
    Returns:
        str or float -- datetime, date as isoformat str. Decimal as float. 
    """
    if isinstance(obj, (datetime, date)):  # for dates and datetimes
        return obj.isoformat()
    elif isinstance(obj, Decimal):  # for amounts returned as Decimal
        return float(obj)
    else:
        raise TypeError("Type %s not serializable" % type(obj))


def remove_nulls_from_list(obj):
    """Recursively removes "None" values from lists. 
    Checks contents of OrderedDicts for lists. 
    Should work with all ConvioAPI output data. 
    Be sure to assign returned value - lower levels already assigned. 
    
    Arguments:
        obj {list or OrderedDict} -- List or OrderedDictionary output by ConvioAPI.
    
    Returns:
        list or OrderedDict -- Returns the object, but with all lists filtered for None values. 
    """
    if isinstance(obj, OrderedDict):
        for key, value in obj.items():
            obj[key] = remove_nulls_from_list(value)
    elif isinstance(obj, list):
        return [remove_nulls_from_list(i) for i in obj if i is not None]
    return obj
