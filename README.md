
## Convio Webservices API


Wrapper around Convio/Luminate webservices SOAP api. Download only. Uses zeep to speak to convio. Uses python 3.6

* Free software: GNU General Public License v3
* API Documentation: http://open.convio.com/webservices/#main
Make sure you whitelist your IP address if you intend to test.


#### Features

* Download only
* Handles pagination
* Handles session
* Built around a few API bugs.

#### To Use

Configure convio/luminate to enable webservices (see convio docs), whitelist your IP address.

Install using pip: `pip install git+https://github.com/CJPBoston/convio_api`


```python
    from convio_api import ConvioAPI
    client = ConvioAPI(username="Username", password="Password", wsdl="example.com/wsdl", partition=1001)
    client.get_incremental_changes_counts('EmailRecipient', change_type='Inserts')
```

#### Credits
zeep
